<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<div class="pt-md pb-md bg-primary">
		<?php get_template_part("/templates/template-parts/footer/client-logos"); ?>
	</div>

	<footer id="colophon" class="site-footer gradient-primary">
		<div id="site-footer" class="clearfix pb-md pt-md pl-xs pr-xs">
			
			<div class="logo-footer">
  				<?php 
					if (get_field('custom_footer_logo', 'option')) {
						?>
							<img src="<?php echo get_field('custom_footer_logo', 'option')['url']; ?>" alt="<?php echo get_field('custom_footer_logo', 'option')['alt']; ?>">
						<?php
					} else{
		  				echo do_shortcode('[lg-site-logo]');
					}
  				?>
  				<hr>
				<?php the_field('footer_logo_content', 'option'); ?>
			</div>
		
			<div class="footer-location-wrap"><?php get_template_part("/templates/template-parts/footer/location"); ?></div>

			<div class="footer-social">
				<?php echo do_shortcode( '[lg-social-media]'); ?>
			</div>

		</div>

		<div id="site-legal">
			<div class="pb-xs pt-xs pl-xs pr-xs">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php get_template_part("/templates/template-parts/page/video-modal"); ?>

<?php wp_footer(); ?>

</body>
</html>
