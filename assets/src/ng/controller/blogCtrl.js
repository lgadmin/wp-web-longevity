module.exports = function($scope, $http, $location) {
	var para = $location.search();

	function init(){
		$scope.category = [];
		$scope.tag = [];
		$scope.blog = [];
		$scope.learn_more_active = false;
		$scope.post_page = 1;

		loadCategoryData();

		if('search' in para){
			$scope.active_search = para['search'];
		}else{
			$scope.active_search = '';
		}
	}

	function loadCategoryData(){
		$http.get('/wp-json/longevity/v1/post_category').then(function(result) {

			$scope.category.push({'name': 'No Category Selected', 'id': -1});

			result.data.map(function(item){
				$scope.category.push({"name": item.name, "id": item.term_id});
			});

			if('category' in para && !isNaN(para['category'])){
				$scope.active_category = parseInt(para['category']);
			}else{
				$scope.active_category = -1;
			}

			loadTagData();

	    }, function(){
	        console.log('error');
	    });
	}

	function loadTagData(){
		$http.get('/wp-json/longevity/v1/post_tag').then(function(result) {

			$scope.tag.push({'name': 'No Tag Selected', 'id': -1});

			result.data.map(function(item){
				$scope.tag.push({"name": item.name, "id": item.term_id});
			});

			if('tag' in para && !isNaN(para['tag'])){
				$scope.active_tag = parseInt(para['tag']);
			}else{
				$scope.active_tag = -1;
			}

			loadBlog();

	    }, function(){
	        console.log('error');
	    });
	}

	function loadBlog(){
		var postUrl = '/wp-json/wp/v2/posts?';

		if($scope.post_page <= 1){
			$scope.blog = [];
		}

		if($scope.active_category != -1){
			postUrl = postUrl + 'categories=' + $scope.active_category + '&';
		}

		if($scope.active_tag != -1){
			postUrl = postUrl + 'tags=' + $scope.active_tag + '&';
		}

		if($scope.active_search != ''){
			postUrl = postUrl + 'search=' + $scope.active_search + '&';
		}

		postUrl = postUrl + 'page=' + $scope.post_page + '&_embed';

		$http.get(postUrl).then(function(result) {
			
			result.data.map(function(item){
				if(item._embedded['wp:featuredmedia']){
					item.featured_media = item._embedded['wp:featuredmedia'][0];
				}

				if(item._embedded['author']){
					item.author = item._embedded['author'][0];

					for(var prop in item.author.avatar_urls) {
					  item.author.avatar_urls = item.author.avatar_urls[prop];
					  break;
					}
				}

				if(item._embedded['wp:term']){
					item.categories = item._embedded['wp:term'][0];
					item.tags = item._embedded['wp:term'][1];
				}

				// Converting Date
				var monthNames = ["January", "February", "March", "April", "May", "June",
				  "July", "August", "September", "October", "November", "December"
				];

				var date = new Date(item.date);
				item.date = {
					'month': monthNames[date.getMonth()],
					'day': date.getDate(),
					'year': date.getFullYear()
				}

				$scope.blog.push(item);
			});

			if(result.data.length == 10){
				$scope.learn_more_active = true;
			}else{
				$scope.learn_more_active = false;
			}

			$location.search('category', $scope.active_category);
			$location.search('tag', $scope.active_tag);
			$location.search('search', $scope.active_search);

	    }, function(){
	        console.log('error');
	    });
	}

	function updateCategory(cat_id){
		$scope.active_category = cat_id;

		resetLoadCategory();
	}

	function resetLoadCategory(){
		$scope.learn_more_active = false;
		$scope.post_page = 1;

		$scope.active_tag = -1;
		$scope.active_search = '';

		loadBlog();
	}

	function resetLoadTag(){
		$scope.learn_more_active = false;
		$scope.post_page = 1;

		$scope.active_category = -1;
		$scope.active_search = '';

		loadBlog();
	}

	function resetLoadSearch(){
		$scope.learn_more_active = false;
		$scope.post_page = 1;

		$scope.active_tag = -1;
		$scope.active_category = -1;

		loadBlog();
	}

	function loadMore(){
		$scope.learn_more_active = false;
		$scope.post_page++;

		loadBlog();
	}

	init();

	$scope.loadBlog = loadBlog;
	$scope.resetLoadCategory = resetLoadCategory;
	$scope.resetLoadTag = resetLoadTag;
	$scope.resetLoadSearch = resetLoadSearch;
	$scope.loadMore = loadMore;
	$scope.updateCategory = updateCategory;
};