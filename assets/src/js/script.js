var feature_overlay_reposition = function(){
	var padding_top = jQuery('#masthead').outerHeight();
	jQuery('.top-feature-section').find('.overlay').css('padding-top', padding_top);
    jQuery('.top-feature-section').find('.overlay').fadeIn();
}

var show_animate = function(array){
    var index = 1;
    array.each(function(){
        var item = jQuery(this);
        setTimeout(function(){
            item.stop().fadeIn(1000);
        }, index * 200);

        index++;
    });
}