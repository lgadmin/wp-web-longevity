// Window Scroll Handler

(function($) {

    $(window).on('scroll', function(){

    	var headerHeight = $('.site-branding').outerHeight() + $('.utility-bar').outerHeight();
    	if($(window).width() < 769){
			$('.max-mega-menu').css('height', ($(window).height() - headerHeight) + 'px');
		}else{
			$('.max-mega-menu').css('height', 'auto');
		}

    })

}(jQuery));