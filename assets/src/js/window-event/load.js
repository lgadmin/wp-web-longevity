// Windows Load Handler

(function($) {

    $(window).on('load', function(){

    	//sticky_header_initialize();
    	//feature_overlay_reposition();

    	var headerHeight = $('.site-branding').outerHeight() + $('.utility-bar').outerHeight();
    	if($(window).width() < 769){
			$('.max-mega-menu').css('height', ($(window).height() - headerHeight) + 'px');
		}else{
			$('.max-mega-menu').css('height', 'auto');
		}

    });

}(jQuery));