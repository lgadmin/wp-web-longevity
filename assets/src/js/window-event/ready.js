// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        $('.dropdown-toggle').closest('li').on('mouseover', function(e){
          $(this).addClass('open');
        });

        $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
          $(this).removeClass('open');
        });

        $('.video-wrapper').find('.play-button').on('click', function(){
          if($(this).hasClass('playing')){
            $(this).removeClass('playing').siblings('video')[0].pause();
          }else{
            $(this).addClass('playing').siblings('video')[0].play();
          }
        });

        //home top banner
        $('.home-top-banner').slick({
          dots: false,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 4000,
          slidesToShow: 1,
          slidesToScroll: 1
        })

        //Testimonial
        $('.testimonial-list.partial').slick({
          dots: true,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 30000,
          speed: 1200,
          cssEase: 'linear',
          slidesToShow: 2,
          slidesToScroll: 2,
          responsive: [
          {
            breakpoint: 1000,
            settings: {
              dots: false,
              arrows: false,
              speed: 500,
              cssEase: 'linear',
              slidesToShow: 1,
              slidesToScroll: 1,
              adaptiveHeight: true,
            }
          }
        ]
        });

        $('.client-logo').slick({
          dots: true,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 2000,
          speed: 600,
          cssEase: 'ease-out',
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
          {
            breakpoint: 1000,
            settings: {
              cssEase: 'linear',
              slidesToShow: 4,
            }
          },
          {
            breakpoint: 768,
            settings: {
              cssEase: 'linear',
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
              dots: false,
              cssEase: 'linear',
              slidesToShow: 2,
            }
          }
        ]
        });


      //If Mega Menu
      $('.mobile-toggle').on('click', function(){
        $('.mega-menu-toggle').toggleClass('mega-menu-open');
        $('body, html').toggleClass('locked');
      });

      var video_modal = $('#video-modal-wrap');

      //Open video in modal
      $('.modal-video').on('click', function(e){
        console.log('here');

        var src = $(this).attr('href');

        e.preventDefault();

        video_modal.find('.video-modal').addClass('playing').find('.video-in-modal video').attr('src', src).get(0).play();
            video_modal.fadeIn();

      })

      $('.modal-video-close').on('click', function(){
        video_modal.fadeOut();
      });

      //Video Modal inside
      video_modal.find('.play-button').on('click', function(){
        $(this).closest('.video-modal').addClass('playing').find('.video-in-modal video').get(0).play();
      })

      video_modal.find('.video-in-modal video').on('click', function(){
        $(this).get(0).pause();
        $(this).closest('.video-modal').removeClass('playing').find('.play-button');
      });
        
    });

}(jQuery));