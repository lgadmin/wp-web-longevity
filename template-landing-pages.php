<?php /* Template Name: Landing page */ ?>

<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content service-landing-page">
			<main>

				<?php 
			        $group = get_field('top_feature');
			        include(locate_template('/templates/template-parts/header/top-feature-service-landing-page.php'));
				?>

				<?php
					$content = get_field('content');
				?>
				<div class="content">
					<div class="container pt-md pb-md">
						<div>
							<h2 class="h1 mb-xs text-secondary"><?php echo get_the_title(); ?></h2>
							<?php echo $content; ?>
						</div>
						<div>
							<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
						</div>
					</div>
				</div>
				
				<!-- Our Testimonial -->
				<?php get_template_part("/templates/template-parts/footer/site-testimonial"); ?>
				<!-- end Our Testimonial -->
			</main>
		</div>
	</div>

<?php get_footer(); ?>