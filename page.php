<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				<!-- Our Testimonial -->
				<?php get_template_part("/templates/template-parts/layout/flexible-content"); ?>
				<!-- end Our Testimonial -->

				<!-- Our Testimonial -->
				<?php get_template_part("/templates/template-parts/footer/site-testimonial"); ?>
				<!-- end Our Testimonial -->
			</main>
		</div>
	</div>

<?php get_footer(); ?>