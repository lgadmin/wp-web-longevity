<?php

/**
 * Client Role
*/
function lg_client_role(){

  // Remove Default Role
  if( get_role('editor') ){
      remove_role( 'editor' );
  }

  if( get_role('author') ){
      remove_role( 'author' );
  }

  if( get_role('contributor') ){
      remove_role( 'contributor' );
  }

  if( get_role('subscriber') ){
      remove_role( 'subscriber' );
  }

  if( get_role('wpseo_manager') ){
      remove_role( 'wpseo_manager' );
  }

    if( get_role('wpseo_editor') ){
      remove_role( 'wpseo_editor' );
  }

  // Add LG Client Role

  $result = add_role(
      'lg_client',
      __( 'LG Client' ),
      array()
  );

  $role = get_role( 'lg_client' );
  $role->add_cap( 'activate_plugins', false );
  $role->add_cap( 'delete_others_pages', true );
  $role->add_cap( 'delete_others_posts', true );
  $role->add_cap( 'delete_pages', true );
  $role->add_cap( 'delete_posts', true );
  $role->add_cap( 'delete_private_pages', true );
  $role->add_cap( 'delete_private_posts', true );
  $role->add_cap( 'delete_published_pages', true );
  $role->add_cap( 'delete_published_posts', true );
  $role->add_cap( 'edit_dashboard', false );
  $role->add_cap( 'edit_others_pages', true );
  $role->add_cap( 'edit_others_posts', true );
  $role->add_cap( 'edit_pages', true );
  $role->add_cap( 'edit_posts', true );
  $role->add_cap( 'edit_private_pages', true );
  $role->add_cap( 'edit_private_posts', true );
  $role->add_cap( 'edit_published_pages', true );
  $role->add_cap( 'edit_published_posts', true );
  $role->add_cap( 'edit_theme_options', true );
  $role->add_cap( 'export', false );
  $role->add_cap( 'import', false );
  $role->add_cap( 'list_users', false );
  $role->add_cap( 'manage_categories', true );
  $role->add_cap( 'manage_links', true );
  $role->add_cap( 'manage_options', false );
  $role->add_cap( 'moderate_comments', false );
  $role->add_cap( 'promote_users', false );
  $role->add_cap( 'publish_pages', true );
  $role->add_cap( 'publish_posts', true );
  $role->add_cap( 'read_private_pages', true );
  $role->add_cap( 'read_private_posts', true );
  $role->add_cap( 'read', true );
  $role->add_cap( 'remove_users', false );
  $role->add_cap( 'switch_themes', false );
  $role->add_cap( 'upload_files', true );
  $role->add_cap( 'customize', true );
  $role->add_cap( 'delete_site', false );
  $role->add_cap( 'update_core', false );
  $role->add_cap( 'update_plugins', false );
  $role->add_cap( 'update_themes', false );
  $role->add_cap( 'install_plugins', false );
  $role->add_cap( 'install_themes', false );
  $role->add_cap( 'upload_plugins', false );
  $role->add_cap( 'upload_themes', false );
  $role->add_cap( 'delete_themes', false );
  $role->add_cap( 'delete_plugins', false );
  $role->add_cap( 'edit_plugins', false );
  $role->add_cap( 'edit_themes', false );
  $role->add_cap( 'edit_files', false );
  $role->add_cap( 'edit_users', false );
  $role->add_cap( 'create_users', false );
  $role->add_cap( 'delete_users', false );
  $role->add_cap( 'unfiltered_html', false );

}

add_action( 'admin_init', 'lg_client_role');

?>