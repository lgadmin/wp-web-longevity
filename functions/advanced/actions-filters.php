<?php

// Admin Screen Filters

add_action('restrict_manage_posts', 'testimonial_filter_by_category');

function testimonial_filter_by_category() {
  global $typenow;
  $post_type = 'testimonial'; // change to your post type
  $taxonomy  = 'testimonial-category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
    ));
  };
}

add_filter('parse_query', 'testimonial_convert_id_to_term_in_query');
function testimonial_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'testimonial'; // change to your post type
  $taxonomy  = 'testimonial-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}

// Admin Screen Filters

add_action('restrict_manage_posts', 'project_filter_by_category');

function project_filter_by_category() {
  global $typenow;
  $post_type = 'project'; // change to your post type
  $taxonomy  = 'project-category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
    ));
  };
}

add_filter('parse_query', 'project_convert_id_to_term_in_query');
function project_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'project'; // change to your post type
  $taxonomy  = 'project-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}

// Admin Screen Filters

add_action('restrict_manage_posts', 'service_filter_by_category');

function service_filter_by_category() {
  global $typenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  if ($typenow == $post_type) {
    $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
    $info_taxonomy = get_taxonomy($taxonomy);
    wp_dropdown_categories(array(
      'show_option_all' => __("Show All {$info_taxonomy->label}"),
      'taxonomy'        => $taxonomy,
      'name'            => $taxonomy,
      'orderby'         => 'name',
      'selected'        => $selected,
      'show_count'      => true,
      'hide_empty'      => true,
    ));
  };
}

add_filter('parse_query', 'service_convert_id_to_term_in_query');
function service_convert_id_to_term_in_query($query) {
  global $pagenow;
  $post_type = 'service'; // change to your post type
  $taxonomy  = 'service-category'; // change to your taxonomy
  $q_vars    = &$query->query_vars;
  if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
    $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
    $q_vars[$taxonomy] = $term->slug;
  }
}


function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
  
  // remove layout title from text
  $title = '';
  $new_title = get_sub_field('block_title');
  if($new_title){
    return $new_title;
  }else{
    return $title;
  }
  
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);


// Restful API
add_action( 'rest_api_init', function () {
  register_rest_route( 'longevity/v1', '/post_category', array(
    'methods' => 'GET',
    'callback' => 'lg_post_category',
  ) );
} );

function lg_post_category(){
  $categories = get_terms([
      'taxonomy' => 'category',
      'hide_empty' => false,
  ]);

  $response = new WP_REST_Response( $categories );
  return $categories;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'longevity/v1', '/post_tag', array(
    'methods' => 'GET',
    'callback' => 'lg_post_tag',
  ) );
} );

function lg_post_tag(){
  $tags = get_terms([
      'taxonomy' => 'post_tag',
      'hide_empty' => false,
  ]);

  $response = new WP_REST_Response( $tags );
  return $response;
}


add_filter( 'gform_pre_render_2', 'populate_posts' );
add_filter( 'gform_pre_validation_2', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_2', 'populate_posts' );
add_filter( 'gform_admin_pre_render_2', 'populate_posts' );
function populate_posts( $form ) {
 
    foreach ( $form['fields'] as &$field ) {
 
        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
            continue;
        }
 
        // you can add additional parameters here to alter the posts that are retrieved
        // more info: http://codex.wordpress.org/Template_Tags/get_posts
        $posts = get_posts( 'numberposts=-1&post_status=publish&post_type=job-post' );
 
        $choices = array();
 
        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
        }
 
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $field->placeholder = 'Select a job to apply';
        $field->choices = $choices;
 
    }
 
    return $form;
}


// Add our custom permastructures for custom taxonomy and post
add_action( 'wp_loaded', 'add_clinic_permastructure' );
function add_clinic_permastructure() {
  global $wp_rewrite;
  add_permastruct( 'service', 'services/%service-category%/%service%', false );
}
// Make sure that all links on the site, include the related texonomy terms
add_filter( 'post_type_link', 'service_permalinks', 10, 2 );
function service_permalinks( $permalink, $post ) {
  if ( $post->post_type !== 'service' )
    return $permalink;
  $terms = get_the_terms( $post->ID, 'service-category' );
  
  if ( ! $terms )
    return str_replace( '%service-category%/', '', $permalink );
  $post_terms = array();
  foreach ( $terms as $term )
    $post_terms[] = $term->slug;
  return str_replace( '%service-category%', implode( ',', $post_terms ) , $permalink );
}

?>