<?php

function create_taxonomy(){

	register_taxonomy(
		'project-category',
		'project',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'project-category', "with_front" => false ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'service-category',
		'service',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'service-category', "with_front" => false),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'staff-category',
		'staff',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'staff-category', "with_front" => false),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'testimonial-category',
		'testimonial',
		array(
			'label' => __( 'Category' ),
			'rewrite' => array( 'slug' => 'testimonial-category', "with_front" => false),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>