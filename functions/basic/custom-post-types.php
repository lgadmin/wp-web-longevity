<?php

//Custom Post Types
function create_post_type() {

    // SERVICES
    register_post_type( 'service',
        array(
          'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'Service' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'services'),
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // PROJECTS
    register_post_type( 'project',
        array(
          'labels' => array(
            'name' => __( 'Projects' ),
            'singular_name' => __( 'Project' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'portfolios'),
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // TESTIMONIALS
    register_post_type( 'testimonial',
        array(
          'labels' => array(
            'name' => __( 'Testimonials' ),
            'singular_name' => __( 'Testimonial' )
          ),
          'public' => false,
          'has_archive' => false,
          'show_ui' => true,
          "rewrite" => array("with_front" => false, "slug" => 'testimonials'),
          'publicly_queryable' => true,
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // LOCATIONS
    register_post_type( 'location',
        array(
          'labels' => array(
            'name' => __( 'Locations' ),
            'singular_name' => __( 'Location' )
          ),
          'public' => true,
          'has_archive' => false,
          'show_ui' => true,
          'rewrite' => array('slug' => 'contact-us', "with_front" => false),
          'publicly_queryable' => true,
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // STAFF
    register_post_type( 'staff',
        array(
          'labels' => array(
            'name' => __( 'Staffs' ),
            'singular_name' => __( 'Staff' )
          ),
          'public' => false,
          'has_archive' => false,
          'show_ui' => true,
          "rewrite" => array("with_front" => false, "slug" => "team-members"),
          'publicly_queryable' => true,
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // JOB POST
    register_post_type( 'job-post',
        array(
          'labels' => array(
            'name' => __( 'Job Posts' ),
            'singular_name' => __( 'Job Post' )
          ),
          'public' => true,
          'has_archive' => false,
          'show_ui' => true,
          "rewrite" => array("with_front" => false, "slug" => "job-posts"),
          'publicly_queryable' => true,
          'show_in_menu'    => 'longevity',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

}
add_action( 'init', 'create_post_type' );

?>