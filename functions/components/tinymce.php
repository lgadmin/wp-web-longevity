<?php

	// Register format button
	function my_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


	// Insert items to format button
	function my_mce_before_init_insert_formats( $init_array ) { 

		$colors = ['white', 'primary', 'secondary'];
		$title_size = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
		$margin = array(
			(object) array("title"=> "No Padding", "value"=> "none"),
			(object) array("title"=> "Extra Small", "value"=> "xs"),
			(object) array("title"=> "Small", "value"=> "sm"),
			(object) array("title"=> "Medium", "value"=> "md"),
			(object) array("title"=> "Large", "value"=> "lg")
		);

		$color_array = [];
		$button_array = [];
		$title_size_array = [];
		$margin_top = [];
		$margin_bottom = [];

		if($title_size && is_array($title_size)){
			foreach ($title_size as $key => $value) {
				array_push($title_size_array, array(
					'title' => ucwords($value),
		            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a',
		            'classes' => $value
				));
			}
		}

		if($colors && is_array($colors)){
			foreach ($colors as $key => $value) {
				array_push($color_array, array(
					'title' => ucwords($value),
		            'inline' => 'span',
		            'classes' => 'text-'.$value
				));
			}
		}

		if($colors && is_array($colors)){
			foreach ($colors as $key => $value) {
				array_push($button_array, array(
					'title' => ucwords($value),
		            'selector' => 'a',
		            'classes' => 'cta-'.$value
				));
			}
		}

		if($margin && is_array($margin)){
			foreach ($margin as $key => $value) {
				array_push($margin_top, array(
					'title' => $value->title,
		            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
		            'classes' => 'mt-'.$value->value
				));
			}
		}

		if($margin && is_array($margin)){
			foreach ($margin as $key => $value) {
				array_push($margin_bottom, array(
					'title' => $value->title,
		            'selector' => 'div,h1,h2,h3,h4,h5,h6,p,a,ul,ol,blockquote',
		            'classes' => 'mb-'.$value->value
				));
			}
		}


	    $style_formats = array(
	    	array(
	            'title' => 'Title Size',
	            'items' =>  $title_size_array
	        ),
	        array(
	            'title' => 'Colors',
	            'items' =>  $color_array
	        ),
	        array(
	            'title' => 'Buttons',
	            'items' =>  $button_array
	        ),
	        array(
	            'title' => 'Spacing Top',
	            'items' =>  $margin_top
	        ),
	        array(
	            'title' => 'Spacing Bottom',
	            'items' =>  $margin_bottom
	        ),
	        array(
	            'title' => 'Image',
	            'items' =>  array(
	            	array(
						'title' => 'Full Width',
			            'selector' => 'img',
			            'classes' => 'img-full'
					)
	            )
	        ),
	        array(
	            'title' => 'List',
	            'items' =>  array(
	            	array(
						'title' => 'Check Mark',
			            'selector' => 'ul, ol',
			            'classes' => 'check-mark'
					),
					array(
						'title' => 'Horizontal List',
						'selector' => 'ul, ol',
						'classes'	=> 'list-horizontal'
					)
	            )
	        )
	    );

	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array; 
	  
	} 

	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

?>