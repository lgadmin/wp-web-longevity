<?php

	/* Include theme functions */
	$function_path = get_stylesheet_directory() . '/functions';

	$file_path = [
		'/basic/theme-supports.php',
		'/basic/enqueue_styles_scripts.php',
		'/helper.php',
		'/basic/custom-post-types.php',
		'/basic/custom-taxonomy.php',
		'/basic/custom-menus.php',
		// '/advanced/nav-walker.php',
		'/advanced/wp-bootstrap-navwalker.php',
		'/advanced/actions-filters.php',
		'/advanced/client-roles.php',
		'/include/template-override.php',
		'/components/tinymce.php'
	];

	foreach ($file_path as $key => $value) {
		if (file_exists($function_path . $value)) {
		    require_once($function_path . $value);
		}
	}
	/* end */

	
	if ( ! function_exists('lg_write_log')) {
     function lg_write_log ( $log )  {
        if ( is_array( $log ) || is_object( $log ) ) {
           error_log( print_r( $log, true ) );
        } else {
           error_log( $log );
        }
     }
  }




?>