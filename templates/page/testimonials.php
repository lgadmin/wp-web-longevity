<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content portfolio">
			<main>
				
				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				<div class="pt-lg pb-sm">
					<div class="container">
						<!--<?php get_template_part("/templates/template-parts/page/testimonials-filter"); ?>-->
						<?php get_template_part("/templates/template-parts/page/testimonials"); ?>
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>