<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				
				<!-- Flexible Contents -->
				<?php get_template_part("/templates/template-parts/layout/flexible-content"); ?>
				<!-- end Flexible Contents -->

				<!-- Careers List -->
				<div class="pt-lg pb-lg bg-gray-lighter pl-xs pr-xs">
					<div class="container">
						<h2 class="color-secondary center">Current Openings</h2>
						<?php
							$args = array(
						        'showposts'	=> -1,
						        'post_type'		=> 'job-post',
						    );

						    $result = new WP_Query( $args );

						    // Loop
						    if ( $result->have_posts() ) :
						    	?>
						    	
								<div class="careers pt-sm pb-sm">
						    	<?php
						        while( $result->have_posts() ) : $result->the_post(); 
						    	$title = get_the_title();
						    	$link = get_permalink();
						    ?>
						    	
						        <div class="center pt-xs">
						        	<a href="<?php echo $link; ?>"><?php echo $title; ?></a>
						        </div>

								<?php
						        endwhile;
						        ?>
						        </div>
						    <?php else: ?>
						    <p class="center pt-xs">No job openings currently.</p>
						        <?php

						    endif; // End Loop

						    wp_reset_query();
						?>
					</div>
				</div>
				<!-- end Careers List -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>