<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				
				<!-- Team Members -->
				<?php get_template_part("/templates/template-parts/page/contacts"); ?>
				<!-- end Team Members -->

				<!-- Vancouver Map -->
				<?php
					$args = array(
				        'showposts'	=> 1,
				        'post_type'		=> 'location',
				        'name'	=> 'vancouver'
				    );

				    $result = new WP_Query( $args );

				    // Loop
				    if ( $result->have_posts() ) :
				        while( $result->have_posts() ) : $result->the_post(); 
				        	$map = get_field('map', $post->ID);
				    ?>
				    	
				       <?php echo do_shortcode($map); ?>

						<?php
				        endwhile;
				    endif; // End Loop

				    wp_reset_query();
				?>
				<!-- End Vancouver Map -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>