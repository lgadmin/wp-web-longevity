<?php
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/assets/dist/css/app-vendor.min.css'; ?>">
<script src="<?php echo get_template_directory_uri() . '/assets/dist/ng/app.min.js'; ?>"></script>

	<?php 
	    if(!is_singular('post') && !is_singular('job-post')){
	        $group = get_field('top_feature');
	        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
	    }
	?>
				
	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				<div class="blog-main pt-lg pb-lg container pl-xs pr-xs" ng-app="app" ng-controller="blogCtrl">
					<div class="blog-filter pb-md">
						<md-input-container>
							<md-select ng-model="active_category" ng-change="resetLoadCategory()" placeholder="Category">
						    	<md-option ng-value="cat.id" ng-repeat="cat in category">{{ cat.name }}</md-option>
						  	</md-select>
						</md-input-container>
						<md-input-container>
							<md-select ng-model="active_tag" ng-change="resetLoadTag()" placeholder="Tag">
						    	<md-option ng-value="tag.id" ng-repeat="tag in tag">{{ tag.name }}</md-option>
						  	</md-select>
						</md-input-container>
						<md-input-container>
							<div class="active_search">
								<input placeholder="search here." type="text" class="search" ng-model="active_search">
								<a class="search-button" ng-value="active_search" ng-click="resetLoadSearch()"><i class="fa fa-search" aria-hidden="true"></i></a>
							</div>
						</md-input-container>
						
					</div>

					<div class="blog-content">
						<div class="single-blog" ng-repeat="blog in blog">
							<div class="split-image">
								<img ng-src="{{blog.featured_media.media_details.sizes.medium.source_url}}" alt="{{blog.featured_media.alt_text}}">
							</div>
							<div class="split-copy">
								<h2 class="h3" ng-bind-html="blog.title.rendered"></h2>
								<div class="pb-xs">
									{{blog.date.month}} {{blog.date.day}}, {{blog.date.year}} | Category: <span ng-init="lastone=$index" ng-repeat="cat in blog.categories"><a ng-click="updateCategory(cat.id)">{{cat.name}} </a></span>
								</div>
								<p ng-bind-html="blog.excerpt.rendered"></p>
								<div class="meta buttons mt-sm">
									<div class="author">
										<img ng-src="{{blog.author.avatar_urls}}" alt=""> Blog Post By {{blog.author.name}}
									</div>
									<div class="button">
										<a href="{{blog.link}}" class="cta-secondary" target="_blank">CONTINUE READING</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<a ng-click="loadMore()" ng-if="learn_more_active" class="load-more pt-sm pb-sm pl-xs pr-xs mt-md center bg-gray-lighter">
						LOAD MORE
					</a>
				</div>
			</main>
		</div>
	</div>

<?php get_footer(); ?>