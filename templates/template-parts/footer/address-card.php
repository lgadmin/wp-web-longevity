
<!-- Default Address Stuff -->
<div class="address-card">
	
	<address itemscope="" itemtype="http://schema.org/LocalBusiness">

		<span class="card-map-marker" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
			<span itemprop="streetAddress"><?php echo do_shortcode('[lg-address1]'); ?></span><br>
			<span itemprop="addressLocality"><?php echo do_shortcode('[lg-city]'); ?></span>, <span itemprop="addressRegion"><?php echo do_shortcode('[lg-province]'); ?></span>&nbsp;<span itemprop="postalCode"><?php echo do_shortcode('[lg-postcode]'); ?></span><br>
		</span>

		<br>

		<?php $phone = get_field('company_phone', 'option'); ?>
		<span class="card-map-phone" itemprop="telephone">Phone: <a href="tel:+1<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></a></span><br>

	</address>

</div>

