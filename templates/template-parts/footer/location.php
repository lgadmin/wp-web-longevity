
<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'location',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
		<div class="footer-location">
    	<?php
	        while( $result->have_posts() ) : $result->the_post(); 
		    	$name = get_the_title();
		    	$address = get_field('address');
		    	$phone_number = get_field('phone_number');
		    	$map = get_field('map');
		    ?>
	    	
	        <div>
	        	<div class="subtitle mb-xs text-white"><?php echo $name; ?></div>
	        	<?php if($map): ?>
		        	<div class="map mb-xs"><?php echo do_shortcode($map); ?></div>
		        <?php endif; ?>
	        	<?php if($address): ?>
		        	<div class="address text-white"><?php echo $address; ?></div>
		        <?php endif; ?>
	        	<?php if($phone_number['sales']): ?>
		        	<div class="phone"><a href="tel:<?php echo $phone_number['sales']; ?>"><?php echo $phone_number['sales']; ?></a></div>
		        <?php endif; ?>
	        </div>

			<?php
	        endwhile;
	        ?>
	        </div>
		<?php
	    endif; // End Loop

    wp_reset_query();
?>