
	<?php

	if( have_rows('client_logos', 'option') ):?>
		<div class="client-logo">
		<?php while ( have_rows('client_logos', 'option') ) : the_row(); 
			$logo = get_sub_field('logo');
		?>

			<div><img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>"></div>

	    <?php endwhile;?>
	</div>
	<?php
	else :
	    // no rows found
	endif;

	?>
