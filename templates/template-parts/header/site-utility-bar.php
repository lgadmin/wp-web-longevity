<div class="utility-bar">
  <div class="container">
    <div class="left">
        <div>
          <?php echo do_shortcode('[lg-social-media]'); ?>
        </div>
        <div>
          <?php

          $mainMenu = array(
            'menu'              => 'secondary-nav',
            // 'theme_location'    => 'top-nav',
            'depth'             => 1,
            'container'         => 'div',
          );
          wp_nav_menu($mainMenu);

          ?>
        </div>
    </div>
    <div class="right">
      <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a>
      <a href="tel:<?php echo do_shortcode('[lg-phone-alt]'); ?>"><?php echo do_shortcode('[lg-phone-alt]'); ?></a>
    </div>
  </div>
</div>