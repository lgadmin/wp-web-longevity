<?php
	$slider = $group['slider'];
	$default_image = get_field('top_feature_default_banner', 'option');
	$services = $group['services'];
?>

<div class="top-feature-section">
	<div class="carousel slide carousel-fade" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<?php if($slider && is_array($slider)): ?>
				<?php foreach ($slider as $key => $value): ?>
					<div class="item <?php if($key == 0){echo 'active';} ?>" style="background-image: url('<?php echo $value['image']; ?>');"></div>
				<?php endforeach; ?>
			<?php else: ?>
				<div class="item active" style="background-image: url('<?php echo $default_image; ?>');">
        		</div>
			<?php endif; ?>
	    </div>
	</div>

	
	<div class="overlay services-overlay">
		<div class="container pl-xs pr-xs">
			<h1 class="mb-sm"><?php echo get_the_title(); ?></h1>
			<?php if($services && is_array($services)): ?>
				<div class="services-overlay-list">
					<?php foreach ($services as $key => $value): 
						$icon = $value['icon'];
						$page = $value['page'];
					?>
						<div>
							<a href="<?php echo $page->guid; ?>">
								<img class="service-icon" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
								<p class="text-primary mt-xs text-uppercase"><?php echo $page->post_title; ?></p>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>		
	

</div>



	