<?php if(have_rows('top_feature')) : ?>
    <?php $count = 0?>
    <?php
        $form = get_field('form');
        $badge = get_field('badge');
    ?>
    <div class='home-top-banner'>
        <?php $counter = 0; while(have_rows('top_feature')) : the_row(); $counter++; ?>
            <div class="banner-item <?php echo in_array($counter, array(1,2)) ? 'img-mobile-pos' : '';?>">
                <?php
                    $image = get_sub_field('image');
                    $content = get_sub_field('content');

                ?>
                <?php if($image && is_array($image)) : ?>
                    <div class='home-banner-image'>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                    </div>
                <?php endif; ?>
                <div class='home-banner-content container'>
                    <div class='home-banner-content-wrapper'>
                        <div class='banner-content-left'>
                            <?php if($badge && is_array($badge)) : ?>
                                <div class=featured-award><img src="<?php echo $badge['url']; ?>" alt="<?php echo $badge['alt']; ?>"></div>
                            <?php endif; ?>
                            <?php echo $content; ?>
                        </div>
                        <div class='banner-content-right'>
                            <?php echo do_shortcode($form); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php $count++; ?>
        <?php endwhile; ?>
    </div>
<?php endif; ?>



