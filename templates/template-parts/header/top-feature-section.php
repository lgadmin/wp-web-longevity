<?php
	$title_text = $group['title_text'];
	$buttons = $group['buttons'];
	$slider = $group['slider'];
	$default_image = get_field('top_feature_default_banner', 'option');
	$form_active = $group['form_active'];
	$featured_award = $group['featured_award'];
?>

<div class="top-feature-section">
	<div class="carousel slide carousel-fade" data-ride="carousel" data-interval="8000">
		<div class="carousel-inner" role="listbox">
			<?php if($slider && is_array($slider)): ?>
				<?php foreach ($slider as $key => $value): ?>
					<div class="item <?php if($key == 0){echo 'active';} ?>" style="background-image: url('<?php echo $value['image']; ?>');"></div>
				<?php endforeach; ?>
			<?php else: ?>
				<div class="item active" style="background-image: url('<?php echo $default_image; ?>');">
        		</div>
			<?php endif; ?>
	    </div>
	</div>

	<div class="overlay">
		<div class="container pl-xs pr-xs">
			<div class="left">
				<div class="content">
				<?php if($featured_award): ?>
				<div class="featured-award">
				<img src="<?php echo $featured_award['url']; ?>" alt="<?php echo $featured_award['alt']; ?>" />
				</div>
				<?php endif; ?>
				<?php if($title_text): ?>
					<h1 class="h1"><?php echo $title_text; ?></h1>
				<?php else: ?>
					<h1 class="h1"><?php echo get_the_title(); ?></h1>
				<?php endif; ?>

				<?php get_template_part("/templates/template-parts/page/breadcrumb"); ?>
				</div>
				<?php if($group['buttons_active'] == 1): ?>
				<div class="buttons">
				<?php foreach ($buttons as $key => $value): ?>
					<a href="<?php echo $value['button']['url']; ?>" class="cta-secondary"><?php echo $value['button']['title']; ?></a>
				<?php endforeach; ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="right">
				<?php if($form_active == 1): ?>
					<?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>



	