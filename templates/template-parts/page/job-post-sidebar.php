<div class="post-content-sidebar">
	
	<!-- Recent Posts -->
	<div class="recent-post">
		<h2>Recent Job Postings</h2>
		<ul>
		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'job-post',
		    );

		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<ul class="careers pt-sm pb-sm">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post(); 
		    	$title = get_the_title();
		    	$link = get_permalink();
		    ?>
		    	
		        <li class="pt-xs">
		        	<a href="<?php echo $link; ?>"><?php echo $title; ?></a>
		        </li>

				<?php
		        endwhile;
		        ?>
		        </ul>
		    <?php else: ?>
		    <p class="center pt-xs">No job openings currently.</p>
		        <?php

		    endif; // End Loop

		    wp_reset_query();
		?>
		</ul>
	</div>
	<!-- end Recent Posts -->
</div>