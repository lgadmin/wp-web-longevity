<div class="post-content-sidebar">

	<!-- Blog Filter -->
	<div class="blog-filter">
		<h2>Filters</h2>
		<?php
			$categories = get_categories();
			$tags = get_tags();
		?>

		<?php if(is_array($categories)): ?>
			<select class="category-filter">
			<option value="-1" selected></option>
			<?php foreach ($categories as $key => $cat): ?>
				<option value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
			<?php endforeach; ?>
			</select>
		<?php endif; ?>

		<?php if(is_array($tags)): ?>
			<select class="tag-filter">
			<option value="-1" selected></option>
			<?php foreach ($tags as $key => $tag): ?>
				<option value="<?php echo $tag->term_id; ?>"><?php echo $tag->name; ?></option>
			<?php endforeach; ?>
			</select>
		<?php endif; ?>

		<div class="active_search">
			<input placeholder="search here." type="text" class="search" ng-model="active_search">
			<a class="search-button" ng-click="resetLoadSearch()"><i class="fa fa-search" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- end Blog Filter -->
	
	<!-- Recent Posts -->
	<div class="recent-post">
		<h2>Recent Posts</h2>
		<ul>
		<?php
		    $args = array( 'numberposts' => '5' );
		    $recent_posts = wp_get_recent_posts( $args );
		    foreach( $recent_posts as $recent ){
		        printf( '<li><a href="%1$s">%2$s</a></li>',
		             esc_url( get_permalink( $recent['ID'] ) ),
		             apply_filters( 'the_title', $recent['post_title'], $recent['ID'] )
		         );
		    }
		?>
		</ul>
	</div>
	<!-- end Recent Posts -->
</div>

<script>
	// Windows Ready Handler

(function($) {

    $(document).ready(function(){
    	$('.category-filter').selectric({
		  onChange: function() {
		    window.location.href="/blog?category="+$(this).val()+"&tag=-1&search=&_embedded";
		  }
		});
		$('.tag-filter').selectric({
		  onChange: function() {
		    window.location.href="/blog?tag="+$(this).val()+"&category=-1&search=&_embedded";
		  }
		});

		$('.active_search .search-button').on('click', function(){
			window.location.href="/blog?tag=-1&category=-1&search="+ $(this).siblings('input').val() +"&_embedded";
		});
    });

}(jQuery));
</script>