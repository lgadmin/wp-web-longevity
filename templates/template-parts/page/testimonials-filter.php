<?php

$terms = get_terms( array(
    'taxonomy' => 'testimonial-category',
    'hide_empty' => false,
) );

?>

<?php if($terms && is_array($terms)): ?>
    <div class="tab-filter-b center">
        <div class="filter" slug="all">
            <a>All</a>
        </div>
        <?php foreach ($terms as $key => $value): ?>
            <div class="filter" slug="<?php echo $value->slug; ?>">
                <a><?php echo $value->name; ?></a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>