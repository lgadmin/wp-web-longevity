<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'project',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
		<div class="portfolios">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$main_image = get_field('main_image');
        $hover_image = get_field('hover_image');
    	$url = get_permalink();
    	$terms = get_the_terms($post->ID, 'project-category');
    	$categories = [];

        if($terms && is_array($terms)){
        	foreach ($terms as $key => $value) {
        		array_push($categories, $value->slug);
        	}
        }

    	$categories = join(" ", $categories);
    ?>
    	
        <div class="<?php if($categories){ echo '' . $categories; } ?> all">
        	<a href="<?php echo $url; ?>">
                <!-- <img class="main" src="<?php echo $main_image['url']; ?>" alt="<?php echo $main_image['alt']; ?>"> -->
                <img src="<?php echo $hover_image['url']; ?>" alt="<?php echo $hover_image['alt']; ?>">
            </a>
        </div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();
?>

<script>
(function($) {

    $(document).ready(function(){

        var filter = $('.tab-filter-a');
        var content = $('.portfolios');
        var default_hash = 'all';
        
        //Project Filter
        filter.find('.filter').on('click', function(){
            location.hash = $(this).attr('slug');
        });

        $(window).on('hashchange', function () {

            if(location.hash == '' || !$('.filter[slug="'+ location.hash.substring(1) +'"]')[0]){
                location.hash = default_hash;
            }

            $('.filter[slug="'+ location.hash.substring(1) +'"]').addClass('active').siblings().removeClass('active');
            //content.find('>div').fadeOut(100, function(){
                //show_animate(content.find('.'+location.hash.substring(1)));
            //});
            content.find('>div').hide();
            content.find('.'+location.hash.substring(1)).show();
        }).trigger('hashchange');

    });

}(jQuery));
</script>