<?php
    $current_post = $post->post_name;

	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'location',
    );
    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :?>
    	<div class="location-filter tab-filter-b">
    	<?php
        while( $result->have_posts() ) : $result->the_post();
        	$slug = $post->post_name;
    	?>
			<div class="filter <?php if($slug == $current_post){echo 'active';} ?>">
				<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
			</div>
		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop
    wp_reset_query();
?>

