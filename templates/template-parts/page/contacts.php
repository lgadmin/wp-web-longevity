<div class="pt-lg pb-lg">
	<div class="container">
		<h3 class="subtitle center h3 color-secondary">GET IN TOUCH</h3>
		<h2 class="h2 center">CONTACTS</h2>

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'staff',
		        'tax_query' => array(
					array(
						'taxonomy' => 'staff-category',
						'field'    => 'slug',
						'terms'    => 'employee',
					),
				),
		    );
		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :?>
		    	<div class="staff-list">
		    	<?php
		        while( $result->have_posts() ) : $result->the_post();
		        	$title = get_the_title();
		        	$job_title = get_field('job_title');
		        	$cell = get_field('cell');
		        	$phone = get_field('phone');
		        	$email = get_field('email');
		        	$link = get_permalink();
		    	?>
					<div>
						<div class="split-image">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						</div>
						<div class="split-copy">
							<div>
								<span class="title color-secondary"><?php echo $title; ?></span>
								<?php if($job_title): ?>
								<span> | <?php echo $job_title; ?></span>
								<?php endif; ?>
							</div>

							<?php if($cell): ?>
							<div>Cell: <a href="tel:<?php echo $cell; ?>"><?php echo $cell; ?></a></div>
							<?php endif; ?>

							<?php if($phone): ?>
							<div>Phone: <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></div>
							<?php endif; ?>

							<?php if($email): ?>
							<div>Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
							<?php endif; ?>

							<a class="btn-secondary" href="<?php echo $link; ?>">View Bio</a>
						</div>
					</div>
				<?php
		        endwhile;
		        ?>
		        </div>
		        <?php
		    endif; // End Loop
		    wp_reset_query();
		?>

	</div>
</div>

		<?php
			$args = array(
		        'showposts'	=> -1,
		        'post_type'		=> 'staff',
		        'tax_query' => array(
					array(
						'taxonomy' => 'staff-category',
						'field'    => 'slug',
						'terms'    => 'support',
					),
				),
		    );
		    $result = new WP_Query( $args );

		    // Loop
		    if ( $result->have_posts() ) :?>
			<div class="pb-lg">
				<div class="container">
					<h2 class="h2 center">SUPPORTING STAFF</h2>
			    	<div class="staff-list">
			    	<?php
			        while( $result->have_posts() ) : $result->the_post();
			        	$title = get_the_title();
			        	$job_title = get_field('job_title');
			        	$cell = get_field('cell');
			        	$phone = get_field('phone');
			        	$email = get_field('email');
			    	?>
						<div>
							<div class="split-image">
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
							</div>
							<div class="split-copy">
								<div>
									<span class="title color-secondary"><?php echo $title; ?></span>
									<?php if($job_title): ?>
									<span> | <?php echo $job_title; ?></span>
									<?php endif; ?>
								</div>

								<?php if($cell): ?>
								<div>Cell: <a href="tel:<?php echo $cell; ?>"><?php echo $cell; ?></a></div>
								<?php endif; ?>

								<?php if($phone): ?>
								<div>Phone: <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a></div>
								<?php endif; ?>

								<?php if($email): ?>
								<div>Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></div>
								<?php endif; ?>
							</div>
						</div>
					<?php
			        endwhile;
			        ?>
			        </div> <!-- end of class staff-list -->
				</div>
			</div>
		        <?php
		    endif; // End Loop
		    wp_reset_query();
		?>

