<div class="pt-lg pb-lg bg-gray-lighter">
	<div class="container center">
		<h3 class="subtitle h3 color-secondary">TESTIMONIALS</h3>
		<h2 class="h2 title">WHAT OUR CUSTOMERS SAY</h2>
		<div class="site-testimonial">
		
		<?php
			$args = array(
	            'showposts'	=> -1,
	            'post_type'		=> 'testimonial',
	        );
	        $result = new WP_Query( $args );

	        // Loop
	        if ( $result->have_posts() ) : ?>
	        <div class="testimonial-list partial">
	        <?php
	            while( $result->have_posts() ) : $result->the_post(); 
	        	$thumbnail = get_field('thumbnail');
	        	$content = get_field('content');
	        	$client = get_the_title();
	        	$website = get_field('website');
	        ?>
	        	
	            <div>
	            	<div class="testimonial">
		        	<div class="split-image mb-sm">
		        		<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
		        	</div>
		        	<div class="split-copy">
		        		<?php echo short_string($content, 40) . '...'; ?>
		        		<br><br><br>
		        		<div><?php echo $client; ?></div>
		        		<?php if($website): ?>
						<div><a class="color-secondary" href="<?php echo $website; ?>"><?php echo $website; ?></a></div>
		        		<?php endif; ?>
		        	</div>
		        </div>
	            </div>

				<?php
	            endwhile;
	            ?>
	            </div>
	            <?php
	        endif; // End Loop

	        wp_reset_query();
		?>

		<div class="buttons">
			<a href="/testimonials" class="cta-primary">VIEW TESTIMONIALS</a>
		</div>
	</div>
</div>