<?php
	$slug = $post->post_name;
	$address = get_field('address');
	$phone_number = get_field('phone_number');
	$email = get_field('email');
?>

<div class="content pl-md pr-md pb-sm" slug="<?php echo $slug; ?>">
		<h3 class="subtitle h3 color-secondary">WHERE CAN YOU FIND US?</h3>
		<h2 class="h2"><?php echo get_the_title().' OFFICE'; ?></h2>

		<?php if($address): ?>
			<div class="pt-xs pb-xs">
				<?php echo $address; ?>
			</div>
		<?php endif; ?>

		<?php if($phone_number['sales'] || $phone_number['customer_service'] || $phone_number['toll_free']): ?>
			<div class="pt-xs pb-xs">
			<?php if($phone_number['sales']): ?>
				<div><b>Sales: </b><a href="tel:<?php echo $phone_number['sales']; ?>"><?php echo $phone_number['sales']; ?></a></div>
			<?php endif; ?>

			<?php if($phone_number['customer_service']): ?>
				<div><b>Customer Service: </b><a href="tel:<?php echo $phone_number['customer_service']; ?>"><?php echo $phone_number['customer_service']; ?></a></div>
			<?php endif; ?>

			<?php if($phone_number['toll_free']): ?>
				<div><b>Toll Free: </b><a href="tel:<?php echo $phone_number['toll_free']; ?>"><?php echo $phone_number['toll_free']; ?></a></div>
			<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if($email['sales'] || $email['email']): ?>
			<div class="pt-xs pb-xs">
			<?php if($email['email']): ?>
				<div><b>Email: </b><a href="mailto:<?php echo $email['email']; ?>"><?php echo $email['email']; ?></a></div>
			<?php endif; ?>

			<?php if($email['sales']): ?>
				<div><b>Sales: </b><a href="mailto:<?php echo $email['sales']; ?>"><?php echo $email['sales']; ?></a></div>
			<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>