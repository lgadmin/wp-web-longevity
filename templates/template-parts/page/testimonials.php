<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'testimonial',
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
		<div class="page-testimonial grid pt-sm">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
    	$thumbnail = get_field('thumbnail');
        $content = get_field('content');
        $website = get_field('website');
        $client = get_the_title();
    	$url = get_permalink();
    	$terms = get_the_terms($post->ID, 'testimonial-category');
    	$categories = [];

        if(is_array($terms)){
        	foreach ($terms as $key => $value) {
        		array_push($categories, $value->slug);
        	}
        }

    	$categories = join(" ", $categories);


    ?>
    	
        <div class="grid-item <?php if($categories){ echo '' . $categories; } ?> all">
            <div class="testimonial">
                <div class="split-image mb-sm">
                    <img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
                </div>
                <div class="split-copy">
                    <?php echo $content; ?>
                    <div><?php echo $client; ?></div>
                    <?php if($website): ?>
                    <div><a class="color-secondary" href="http://<?php echo $website; ?>"><?php echo $website; ?></a></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();
?>

<script>
(function($) {

    $(document).ready(function(){

        var filter = $('.tab-filter-b');
        var content = $('.page-testimonial');
        var default_hash = 'all';
        
        //Project Filter
        filter.find('.filter').on('click', function(){
            location.hash = $(this).attr('slug');
        });

        $(window).on('hashchange', function () {

            if(location.hash == '' || !$('.filter[slug="'+ location.hash.substring(1) +'"]')[0]){
                location.hash = default_hash;
            }

            $('.filter[slug="'+ location.hash.substring(1) +'"]').addClass('active').siblings().removeClass('active');
            content.find('>div').css('display', 'none');
            content.find('.'+location.hash.substring(1)).css('display', 'block');

            var grid = $('.grid').masonry({
              // options
              itemSelector: '.grid-item',
            });

        }).trigger('hashchange');

    });

}(jQuery));
</script>