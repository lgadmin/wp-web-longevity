<div class="pt-lg pb-lg center container pl-xs pr-xs">
	<h3 class="subtitle h3 color-secondary">OUR PORTFOLIO</h3>
	<h2 class="h2 title pb-xs">OUR LATEST PROJECTS</h2>

	<?php
		$args = array( 
	        'showposts'	=> 6,
	        'post_type'		=> 'project',
	    );
	    $result = new WP_Query( $args );

	    // Loop
	    if ( $result->have_posts() ) :
	    	?>
			<div class="portfolios">
	    	<?php
	        while( $result->have_posts() ) : $result->the_post(); 
	    	$thumbnail = get_the_post_thumbnail_url();
	    	$url = get_permalink();
	    	$terms = get_the_terms($post->ID, 'project-category');
	    	$categories = [];

	    	if($terms && is_array($terms)){
		    	foreach ($terms as $key => $value) {
		    		array_push($categories, $value->slug);
		    	}
	    	}

	    	$categories = join(" ", $categories);
	    ?>
	    	
	        <div <?php if($categories){ echo 'class="' . $categories . '"'; } ?>>
	        	<a href="<?php echo $url; ?>"><img src="<?php echo get_field('hover_image')['url']; ?>" alt=""></a>
	        </div>

			<?php
	        endwhile;
	        ?>
	        </div>
	        <?php
	    endif; // End Loop

	    wp_reset_query();
	?>

	<div class="buttons"><a href="/portfolio" class="cta-secondary">VIEW PORTFOLIO</a></div>
</div>
