
<?php 

	$terms_html = '';
	$terms = get_terms( array(
				'taxonomy'	=> array(
					'project-category',
					'service-category',
					'staff-category',
					'testimonial-category'
					),
		    	'hide_empty' => false,
			));

	if($terms && is_array($terms)){
		foreach ($terms as $key => $term) {
			if(has_term($term->term_id, $term->taxonomy)){
				$link = get_term_link($term->term_id);
				$title = $term->name;

				$terms_html .= "<a href='$link'>$title</a><span class='term-seprator'>, </span>";
			}
		}
	}

?>

<?php if(!is_front_page()): ?>

	<div class="pt-xs pb-xs container lg-breadcrumb">
		<a href="/">Home</a>

		<?php if($post->post_type == 'project'): ?>
			<?php echo ' -> <a href="/portfolio">Portfolios</a>'; ?>
		<?php elseif($post->post_type == 'staff'): ?>
			<?php echo ' -> <a href="/our-team">Team Members</a>'; ?>
		<?php endif; ?>

		<?php if($terms_html && $post->post_type == 'service'): ?>
			<?php echo ' -> <span>' .  $terms_html . '</span>'; ?>
		<?php endif; ?>

		-> <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a>
	</div>
<?php endif; ?>