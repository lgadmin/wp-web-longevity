<?php
	$subtitle = get_sub_field('subtitle');
	$title = get_sub_field('title');
	$table = get_sub_field('table');
?>

<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

	<!-- Component Code -->
	<div>
		<?php if($subtitle): ?>
		<h3 class="subtitle color-secondary h3 center">
			<?php echo $subtitle; ?>
		</h3>
		<?php endif; ?>

		<?php if($title): ?>
		<h2 class="h2 center pb-xs">
			<?php echo $title; ?>
		</h2>
		<?php endif; ?>

		<div class="container-xs">
			<?php if($table): ?>
				<div class="lg-table ad-on">
					<?php
						$row = sizeof($table['body']);
						$column = sizeof($table['body'][0]);
					?>
					<div class="table-column">
						<div class="column-content">
							<div class="column-body">
								<div class="column-body-content">
									<?php for($y = 0; $y < $row; $y++): ?>
										<div>
											<?php for($x = 0; $x < $column; $x++): ?>
												<div>
													<?php if($x % 2 == 1): ?>
														<div class="background-wrapper"></div>
													<?php endif; ?>
													<?php echo $table['body'][$y][$x]['c']; ?>		
												</div>
											<?php endfor; ?>
										</div>
									<?php endfor; ?>
								</div>
							</div>
						</div>	
					</div>
				</div>
				<div class="buttons pb-sm"><a href="/contact-us/vancouver/" class="cta-primary">SIGN UP</a></div>
			<?php endif; ?>
		</div>
	</div>
	<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>
