<?php
	$icon = get_sub_field('icon');
	$button = get_sub_field('button');
	$title = get_sub_field('title');
	$content = get_sub_field('content');
?>

<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

	<!-- Component Code -->
	<div class="split-image">
		<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>">
	</div>
	<div class="split-copy">
		<?php if($title): ?>
			<h2 class="h1 color-secondary"><?php echo $title; ?></h2>
		<?php endif; ?>
		<?php echo $content; ?>

		<?php if($button): ?>
		<div class="center">
			<a href="<?php echo $button['url']; ?>" class="cta-primary mt-xs"><?php echo $button['title']; ?></a>
		</div>
	<?php endif; ?>
	</div>
	<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>
