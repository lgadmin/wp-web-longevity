<?php
	$video = get_sub_field('video');
	$thumbnail = get_sub_field('thumbnail');
?>

<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

<!-- Component Code -->
<div class="video">
	<img src="<?php echo $thumbnail['url']; ?>" alt="<?php echo $thumbnail['alt']; ?>">
	<div class="play-button modal-video" href="<?php echo $video; ?>">
		<i class="fa fa-play-circle" aria-hidden="true"></i>
	</div>
</div>
<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>

