<?php

$block_width = get_sub_field('block_width');
$block_class = get_sub_field('block_class');
$background_image = get_sub_field('background_image');
$image = get_sub_field('image');

$background_color = get_sub_field('background_color');
$background_color_array = array("White"=>"", "Light Gray"=>"bg-gray-lighter");

$block_padding = get_sub_field('block_padding');
$block_padding_array = array(
	"Large Top"=>"pt-lg",
	"Large Bottom"=>"pb-lg",
	"Large Left"=>"pl-lg",
	"Large Right"=>"pr-lg",
	"Medium Top"=>"pt-md",
	"Medium Bottom"=>"pb-md",
	"Medium Left"=>"pl-md",
	"Medium Right"=>"pr-md",
	"Small Top"=>"pt-sm",
	"Small Bottom"=>"pb-sm",
	"Small Left"=>"pl-sm",
	"Small Right"=>"pr-sm",
	"Extra Small Top"=>"pt-xs",
	"Extra Small Bottom"=>"pb-xs",
	"Extra Small Left"=>"pl-xs",
	"Extra Small Right"=>"pr-xs",
	);

$block_margin = get_sub_field('block_margin');
$block_margin_array = array(
	"Large Top"=>"mt-lg",
	"Large Bottom"=>"mb-lg",
	"Large Left"=>"ml-lg",
	"Large Right"=>"mr-lg",
	"Medium Top"=>"mt-md",
	"Medium Bottom"=>"mb-md",
	"Medium Left"=>"ml-md",
	"Medium Right"=>"mr-md",
	"Small Top"=>"mt-sm",
	"Small Bottom"=>"mb-sm",
	"Small Left"=>"ml-sm",
	"Small Right"=>"mr-sm",
	"Extra Small Top"=>"mt-xs",
	"Extra Small Bottom"=>"mb-xs",
	"Extra Small Left"=>"ml-xs",
	"Extra Small Right"=>"mr-xs",
	);


if(!function_exists('lg_single_select')){
	function lg_single_select($value, $array){
		if($value){
			return $array[$value];
		}else{
			return '';
		}
	}
}

if(!function_exists('lg_multi_select')){
	function lg_multi_select($values, $array){
		if($values){
			foreach ($values as $key => $value) {
				if($array[$value] && $array[$value] != null){
					echo $array[$value]. ' ';
				}
			}
		}
	}
}

?>


<div class="<?php echo $block_class; ?> 
			<?php echo lg_single_select($background_color, $background_color_array); ?> 
			<?php if($block_padding['active_padding'] == 1): ?>
			<?php echo lg_multi_select($block_padding['block_padding'], $block_padding_array); ?> 
			<?php endif; ?>
			<?php if($block_margin['active_margin'] == 1): ?>
			<?php echo lg_multi_select($block_margin['block_margin'], $block_margin_array); ?>
			<?php endif; ?>
			<?php if($background_image): ?>
			<?php echo 'intro-bg'; ?>
			<?php endif; ?>
			"
	<?php if($background_image): ?>
	 style="background-image: url('<?php echo $background_image; ?>');"
	<?php endif; ?>
>
	<?php if($block_width == 'Container Width'): ?>
	<div class="container">
	<?php endif; ?>
		<div class="flexible-content">