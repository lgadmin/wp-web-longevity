<?php
	$image = get_sub_field('image');
	$title_primary = get_sub_field('title_primary');
	$title_secondary = get_sub_field('title_secondary');
	$button = get_sub_field('button');
	$content = get_sub_field('content');
	$reverse = get_sub_field('reverse');
?>

<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

	<!-- Component Code -->
	<div class="split-content <?php if($reverse == 1){ echo 'reverse'; } ?>">
		<div class="split-image">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
		</div>
		<div class="split-copy">
			<?php if($title_primary): ?>
				<h2><?php echo $title_primary; ?></h2>
			<?php endif; ?>

			<?php if($title_secondary): ?>
				<h3 class="color-secondary subtitle"><?php echo $title_secondary; ?></h3>
			<?php endif; ?>

			<?php echo $content; ?>

			<?php if($button): ?>
			<div class="buttons">
				<a href="<?php echo $button['url']; ?>" class="cta-primary"><?php echo $button['title']; ?></a>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>
