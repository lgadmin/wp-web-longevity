<?php
	$subtitle = get_sub_field('subtitle');
	$title = get_sub_field('title');
	$content = get_sub_field('content');
	$ctas = get_sub_field('ctas');
?>

<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

	<!-- Component Code -->
	<div class="intro-section">
		<?php if($subtitle): ?>
		<h3 class="subtitle color-secondary h3 center">
			<?php echo $subtitle; ?>
		</h3>
		<?php endif; ?>

		<?php if($title): ?>
		<h2 class="h2 center pb-xs">
			<?php echo $title; ?>
		</h2>
		<?php endif; ?>

		<div class="container-xs">
			<?php echo $content; ?>
			<?php if($ctas): ?>
				<div class="buttons">
				<?php foreach ($ctas as $key => $value): ?>
					<a href="<?php echo $value['button']['url']; ?>" class="cta-primary"><?php echo $value['button']['title']; ?></a>
				<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>
