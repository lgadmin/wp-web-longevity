<?php

// check if the flexible content field has rows of data
if( have_rows('block_sections') ):
     // loop through the rows of data
    while ( have_rows('block_sections') ) : the_row();
	switch (get_row_layout()) {
		case 'text_editor':
			include(locate_template('/templates/template-parts/layout/text-editor.php'));
			break;
		case 'intro_section':
			include(locate_template('/templates/template-parts/layout/intro-section.php'));
			break;
		case 'video':
			include(locate_template('/templates/template-parts/layout/video.php'));
			break;
		case 'intro_with_icon':
			include(locate_template('/templates/template-parts/layout/intro-with-icon.php'));
			break;
		case 'split_content':
			include(locate_template('/templates/template-parts/layout/split-content.php'));
			break;
		case 'package':
			include(locate_template('/templates/template-parts/layout/package.php'));
			break;
		case 'package_ad-ons':
			include(locate_template('/templates/template-parts/layout/package-ad-ons.php'));
			break;
		case 'awards_block':
			include(locate_template('/templates/template-parts/layout/awards-block.php'));
			break;
	}

    endwhile;
else :
    // no layouts found
endif;
?>