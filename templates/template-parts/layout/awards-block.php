
<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

<!-- Component Code -->
<?php if(have_rows("longevity_awards")) : ?>
<h2 class="h2 center text-uppercase awards-title">awards</h2>
<div class="longevity-awards">
	<?php while(have_rows("longevity_awards")) :the_row(); ?>
		<?php 
			$award_image = get_sub_field("award_image");
			$award_link = get_sub_field("award_link");
		 ?>
		 <a href="<?php echo $award_link; ?>" target="_blank"><img src="<?php echo $award_image['url']; ?>" alt="<?php echo $award_image['alt']; ?>" /></a>
	<?php endwhile; ?>
</div>
<?php endif; ?>
<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>

