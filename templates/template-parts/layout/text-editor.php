<?php
	$content = get_sub_field('content');
?>
<?php include(locate_template('/templates/template-parts/layout/component-before.php')); ?>

<!-- Component Code -->
<div class="text-content">
	<?php echo $content; ?>
</div>
<!-- end Component Code -->

<?php include(locate_template('/templates/template-parts/layout/component-after.php')); ?>

