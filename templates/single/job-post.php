<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container">
				<div class="pt-xs pb-lg post-main pl-sm pr-sm">
					<h1 class="h2"><?php echo get_the_title(); ?></h1>

					<div class="post-thumbnail pt-xs mb-sm">
						<?php $job_banner = get_field("job_post_banner_image") ?>
						<?php if ($job_banner ): ?>	
							<img src="<?php echo $job_banner['url']; ?>" alt="<?php echo $job_banner['alt']; ?>">
						<?php else: ?>
							<img src="<?php echo get_stylesheet_directory_uri().'/assets/dist/images/job_banner.jpg'; ?>" alt="">
						<?php endif; ?>

					</div>

					<div class="post-content">

						<!-- Post Main Content -->
						<div class="post-content-main">
							<?php if (have_posts()) : while (have_posts()) : the_post(); 
								
							?>
								<div class="text-content">
									<?php the_content(); ?>
								</div>
								<div class="job-form pt-md">
									<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]'); ?>
								</div>
							<?php endwhile; endif; ?>
						</div>
						<!-- end Post Main Content -->

						<!-- Post Side Bar -->
						<?php get_template_part("/templates/template-parts/page/job-post-sidebar"); ?>
						<!-- end Post Side Bar -->
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>