<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				
				<div class="pt-lg pb-lg container bio">
					<div class="split-image">
						<img src="<?php echo get_the_post_thumbnail_url(); ?>">
					</div>
					<div class="split-copy">
						<?php the_field('bio'); ?>
					</div>
				</div>


				<?php
					$phone = get_field('phone');
					$job_title = get_field('job_title');
					$email = get_field('email');
				?>
				<div class="pt-lg pb-lg staff-bio-contact">
					<div class="container">
						<?php the_title(); ?> <?php if($job_title): ?>| <?php echo $job_title; ?><?php endif; ?>
						<?php if($phone): ?>
							<br>
							Phone: <a href="tel:<?php echo $phone; ?>"><?php echo $phone; ?></a>
						<?php endif; ?>
						<?php if($email): ?>
							<br>
							Email: <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a>
						<?php endif; ?>
					</div>
				</div>

				<!-- Team Members -->
				<?php get_template_part("/templates/template-parts/page/contacts"); ?>
				<!-- end Team Members -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>