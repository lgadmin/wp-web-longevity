<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				
				<div class="container pt-lg pb-lg">
					<?php
						$project_logo = get_field('project_logo');
						$the_problem = get_field('the_problem');
						$the_solution = get_field('the_solution');
						$what_we_did = get_field('what_we_did');
						$web_link = get_field('web_link');
						$terms = get_the_terms($post->ID, 'project-category');
						$title = get_the_title();

						$subtitle = '';
						if(is_array($terms)){
							foreach ($terms as $key => $value) {
								if($key != 0){
									$subtitle .= ' | ';
								}

								$subtitle .= $value->name;
							}
						}
					?>

					<?php if($subtitle): ?>
						<h2 class="h3 subtitle color-secondary center">
							<?php echo $subtitle; ?>
						</h2>
					<?php endif; ?>
					<h3 class="h2 center">
						<?php echo $title; ?>
					</h3>
					<div class="center project-logo">
						<img src="<?php echo $project_logo['url']; ?>" alt="<?php echo $project_logo['alt']; ?>">
					</div>

					<div class="project-gallery pt-md pb-xs">
						<div class="split-image">
							<?php
							if( have_rows('project_images') ):
								?>
								<div class="project-images">
								<?php
							    while ( have_rows('project_images') ) : the_row();
							        $image = get_sub_field('image');
							        ?>
									<div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
							        <?php
							    endwhile;
							    ?>
							    </div>
							    <?php
							else :
							    // no rows found
							endif;
							?>
						</div>
						<div class="split-copy">
							<section class="pb-xs">
								<h3 class="h3 color-secondary">
									THE PROBLEM
								</h3>
								<?php echo $the_problem; ?>
							</section>
							<section class="pb-xs">
								<h3 class="h3 color-secondary">
									THE SOLUTION
								</h3>
								<?php echo $the_solution; ?>
							</section>
							<section>
								<h3 class="h3 color-secondary">
									WHAT WE DID
								</h3>
								<?php echo $what_we_did; ?>
							</section>
							<?php if($web_link): ?>
								<div class="buttons">
									<a href="<?php echo $web_link['url']; ?>" class="cta-secondary" target="<?php echo $web_link['target']; ?>"><?php echo $web_link['title']; ?></a>
								</div>
							<?php endif; ?>
						</div>
					</div>
				</div>

			</main>
		</div>
	</div>

	<script>
		(function($) {

		    $(document).ready(function(){

		        $('.project-images').slick({
		        	dots:true,
		        	arrows:false
		        });

		    });

		}(jQuery));
	</script>

<?php get_footer(); ?>