<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php 
				    if(!is_singular('post') && !is_singular('job-post')){
				        $group = get_field('top_feature');
				        include(locate_template('/templates/template-parts/header/top-feature-section.php'));
				    }
				?>
				
				<div class="pt-lg pb-lg">
					<div class="container">
						<?php get_template_part("/templates/template-parts/page/locations-filter"); ?>

						<div class="location-content pt-sm half-block reverse">
							<div><?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?></div>
							<div><?php get_template_part("/templates/template-parts/page/locations-content"); ?></div>
						</div>
					</div>
				</div>

				<!-- Google Map -->
				<?php
					$map = get_field('map');
				?>
				<?php echo do_shortcode($map); ?>
				<!-- end Google Map -->

				<!-- Contacts -->
				<?php get_template_part("/templates/template-parts/page/contacts"); ?>
				<!-- end Contacts -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>