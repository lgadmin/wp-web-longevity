<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main class="container">
				<?php
					$categories = get_the_category($post->ID);
					$tags = get_the_tags($post->ID);

					$category = '';
					$tag = '';

					if(is_array($categories)){
						$i = 0;
						$len = count($categories);
						foreach ($categories as $key => $cat) {
							if($i == $len - 1){
								$category .= '<a href="/blog?category='. $cat->term_id .'">'. $cat->name .'</a>';
							}else{
								$category .= '<a href="/blog?category='. $cat->term_id .'">'. $cat->name .'</a>, ';
							}
							$i++;
						}
					}
				?>
				<div class="pt-xs pb-lg post-main pl-sm pr-sm">
					<h1 class="h2"><?php echo get_the_title(); ?></h1>
					<div><?php echo get_the_date(); ?> | Categories: <?php echo $category; ?></div>
					<div class="author pt-xs"><?php echo get_avatar($post->post_author); ?> <span class="pl-xs">Blog Post By <?php the_author_meta('display_name', (int)$post->post_author); ?></span></div>

					<div class="post-thumbnail pt-xs mb-sm">
						<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
					</div>

					<div class="post-content">

						<!-- Post Main Content -->
						<div class="post-content-main">
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
								<div class="text-content">
									<?php the_content(); ?>
								</div>
							<?php endwhile; endif; ?>
						</div>
						<!-- end Post Main Content -->

						<!-- Post Side Bar -->
						<?php get_template_part("/templates/template-parts/page/post-sidebar"); ?>
						<!-- end Post Side Bar -->
					</div>
				</div>

			</main>
		</div>
	</div>

<?php get_footer(); ?>