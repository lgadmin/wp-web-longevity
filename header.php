<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://cdn.callrail.com">
    <link rel="preconnect" href="https://www.googleadservices.com">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://www.google-analytics.com">
    <link rel="preconnect" href="https://www.google.ca">
    <link rel="preconnect" href="https://googleads.g.doubleclick.net">
    <link rel="preconnect" href="https://www.gstatic.com">
    <link rel="preconnect" href="https://www.googletagmanager.com">
    <link rel="preconnect" href="https://maps.googleapis.com">
    <link rel="preconnect" href="https://www.google.com">


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header <?php if(!is_singular('post') && !is_singular('job-post')){echo 'sticky-header';} ?>">
		<?php if(get_field('show_announcement_bar', 'options') and get_field('announcement_bar_content', 'options')): ?>
			<div class="announcement-bar">
				<?php echo get_field('announcement_bar_content', 'options') ?>
			</div> 
		<?php endif; ?>
    <div class="header-content">

      <?php get_template_part("/templates/template-parts/header/site-utility-bar"); ?>

      <div class="header-main">
        <div class="container">
      		<div class="site-branding">
      			<div class="logo">
      				<?php
                $responsive_logo = get_field('mobile_logo','option');
      					echo do_shortcode('[lg-site-logo]');

                if ($responsive_logo) : ?>
                <a href="/" class='responsive-logo' rel="home" itemprop="url">
                  <img src="<?php echo $responsive_logo['url'] ?>" alt="<?php echo $responsive_logo['alt']; ?>">
                </a>
                <?php endif; ?>
      			</div>

            <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
      		</div><!-- .site-branding -->

          <div class="main-navigation">
            	<nav class="navbar navbar-default">
            	    <!-- Brand and toggle get grouped for better mobile display -->
            	    <div class="navbar-header">
            	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
            	        <span class="sr-only">Toggle navigation</span>
            	        <span class="icon-bar"></span>
            	        <span class="icon-bar"></span>
            	        <span class="icon-bar"></span>
            	      </button>
            	    </div>


                  <div class="nav-desktop"><?php echo do_shortcode('[maxmegamenu location=primary-nav]'); ?></div>
                  <!-- Main Menu  -->
                  <div class="nav-mobile"><?php echo do_shortcode('[maxmegamenu location=max_mega_menu_1]'); ?></div>
            	</nav>
          </div>

        </div>
      </div>
    </div>

  </header><!-- #masthead -->
